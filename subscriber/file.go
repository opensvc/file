package subscriber

import (
	"context"
	log "github.com/micro/go-micro/v2/logger"

	file "github.com/pku-hit/file/proto/file"
)

type File struct{}

func (e *File) Handle(ctx context.Context, msg *file.Message) error {
	log.Info("Handler Received message: ", msg.Say)
	return nil
}

func Handler(ctx context.Context, msg *file.Message) error {
	log.Info("Function Received message: ", msg.Say)
	return nil
}
