
GOPATH:=$(shell go env GOPATH)
MODIFY=Mgithub.com/micro/go-micro/api/proto/api.proto=github.com/micro/go-micro/v2/api/proto

.PHONY: proto
proto:
    
	protoc --proto_path=${GOPATH}/src:. --micro_out=. --go_out=plugins=grpc:. proto/*.proto
    

.PHONY: build
build: proto

	go build -o file-service *.go

.PHONY: test
test:
	go test -v ./... -cover

.PHONY: docker
docker:
	docker build . -t file-service:latest
