// Code generated by protoc-gen-micro. DO NOT EDIT.
// source: proto/file.proto

package proto

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	empty "github.com/golang/protobuf/ptypes/empty"
	libresp "github.com/pku-hit/libresp"
	math "math"
)

import (
	context "context"
	api "github.com/micro/go-micro/v2/api"
	client "github.com/micro/go-micro/v2/client"
	server "github.com/micro/go-micro/v2/server"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

// Reference imports to suppress errors if they are not otherwise used.
var _ api.Endpoint
var _ context.Context
var _ client.Option
var _ server.Option

// Api Endpoints for File service

func NewFileEndpoints() []*api.Endpoint {
	return []*api.Endpoint{}
}

// Client API for File service

type FileService interface {
	// 上传临时文件，指定缓存时间，文件是临时状态，过期清理
	UploadTempFile(ctx context.Context, in *empty.Empty, opts ...client.CallOption) (*libresp.GenericResponse, error)
	// 持久化临时文件，返回文件信息
	PersisitFile(ctx context.Context, in *empty.Empty, opts ...client.CallOption) (*libresp.GenericResponse, error)
	// 上传文件，直接持久化
	UploadFile(ctx context.Context, in *empty.Empty, opts ...client.CallOption) (*libresp.GenericResponse, error)
	// 通过ID获取文件信息
	GetFileInfo(ctx context.Context, in *empty.Empty, opts ...client.CallOption) (*libresp.GenericResponse, error)
}

type fileService struct {
	c    client.Client
	name string
}

func NewFileService(name string, c client.Client) FileService {
	return &fileService{
		c:    c,
		name: name,
	}
}

func (c *fileService) UploadTempFile(ctx context.Context, in *empty.Empty, opts ...client.CallOption) (*libresp.GenericResponse, error) {
	req := c.c.NewRequest(c.name, "File.UploadTempFile", in)
	out := new(libresp.GenericResponse)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *fileService) PersisitFile(ctx context.Context, in *empty.Empty, opts ...client.CallOption) (*libresp.GenericResponse, error) {
	req := c.c.NewRequest(c.name, "File.PersisitFile", in)
	out := new(libresp.GenericResponse)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *fileService) UploadFile(ctx context.Context, in *empty.Empty, opts ...client.CallOption) (*libresp.GenericResponse, error) {
	req := c.c.NewRequest(c.name, "File.UploadFile", in)
	out := new(libresp.GenericResponse)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *fileService) GetFileInfo(ctx context.Context, in *empty.Empty, opts ...client.CallOption) (*libresp.GenericResponse, error) {
	req := c.c.NewRequest(c.name, "File.GetFileInfo", in)
	out := new(libresp.GenericResponse)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// Server API for File service

type FileHandler interface {
	// 上传临时文件，指定缓存时间，文件是临时状态，过期清理
	UploadTempFile(context.Context, *empty.Empty, *libresp.GenericResponse) error
	// 持久化临时文件，返回文件信息
	PersisitFile(context.Context, *empty.Empty, *libresp.GenericResponse) error
	// 上传文件，直接持久化
	UploadFile(context.Context, *empty.Empty, *libresp.GenericResponse) error
	// 通过ID获取文件信息
	GetFileInfo(context.Context, *empty.Empty, *libresp.GenericResponse) error
}

func RegisterFileHandler(s server.Server, hdlr FileHandler, opts ...server.HandlerOption) error {
	type file interface {
		UploadTempFile(ctx context.Context, in *empty.Empty, out *libresp.GenericResponse) error
		PersisitFile(ctx context.Context, in *empty.Empty, out *libresp.GenericResponse) error
		UploadFile(ctx context.Context, in *empty.Empty, out *libresp.GenericResponse) error
		GetFileInfo(ctx context.Context, in *empty.Empty, out *libresp.GenericResponse) error
	}
	type File struct {
		file
	}
	h := &fileHandler{hdlr}
	return s.Handle(s.NewHandler(&File{h}, opts...))
}

type fileHandler struct {
	FileHandler
}

func (h *fileHandler) UploadTempFile(ctx context.Context, in *empty.Empty, out *libresp.GenericResponse) error {
	return h.FileHandler.UploadTempFile(ctx, in, out)
}

func (h *fileHandler) PersisitFile(ctx context.Context, in *empty.Empty, out *libresp.GenericResponse) error {
	return h.FileHandler.PersisitFile(ctx, in, out)
}

func (h *fileHandler) UploadFile(ctx context.Context, in *empty.Empty, out *libresp.GenericResponse) error {
	return h.FileHandler.UploadFile(ctx, in, out)
}

func (h *fileHandler) GetFileInfo(ctx context.Context, in *empty.Empty, out *libresp.GenericResponse) error {
	return h.FileHandler.GetFileInfo(ctx, in, out)
}
