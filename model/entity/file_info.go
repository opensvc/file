package entity

import "time"

type FileInfo struct {
	ID string `gorm:"primary_key;type:varchar(32)"`

	CreateAt *time.Time `gorm:"create_at"`
	UpdateAt *time.Time `gorm:"update_at"`
	DeleteAt *time.Time `gorm:"delete_at"`
}
