package handler

import (
	"context"

	log "github.com/micro/go-micro/v2/logger"

	file "github.com/pku-hit/file/proto/file"
)

type File struct{}

// Call is a single request handler called via client.Call or the generated client code
func (e *File) Call(ctx context.Context, req *file.Request, rsp *file.Response) error {
	log.Info("Received File.Call request")
	rsp.Msg = "Hello " + req.Name
	return nil
}

// Stream is a server side stream handler called via client.Stream or the generated client code
func (e *File) Stream(ctx context.Context, req *file.StreamingRequest, stream file.File_StreamStream) error {
	log.Infof("Received File.Stream request with count: %d", req.Count)

	for i := 0; i < int(req.Count); i++ {
		log.Infof("Responding: %d", i)
		if err := stream.Send(&file.StreamingResponse{
			Count: int64(i),
		}); err != nil {
			return err
		}
	}

	return nil
}

// PingPong is a bidirectional stream handler called via client.Stream or the generated client code
func (e *File) PingPong(ctx context.Context, stream file.File_PingPongStream) error {
	for {
		req, err := stream.Recv()
		if err != nil {
			return err
		}
		log.Infof("Got ping %v", req.Stroke)
		if err := stream.Send(&file.Pong{Stroke: req.Stroke}); err != nil {
			return err
		}
	}
}
