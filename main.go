package main

import (
	log "github.com/micro/go-micro/v2/logger"
	"github.com/micro/go-micro/v2"
	"github.com/pku-hit/file/handler"
	"github.com/pku-hit/file/subscriber"

	file "github.com/pku-hit/file/proto/file"
)

func main() {
	// New Service
	service := micro.NewService(
		micro.Name("go.micro.service.file"),
		micro.Version("latest"),
	)

	// Initialise service
	service.Init()

	// Register Handler
	file.RegisterFileHandler(service.Server(), new(handler.File))

	// Register Struct as Subscriber
	micro.RegisterSubscriber("go.micro.service.file", service.Server(), new(subscriber.File))

	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
